{
  description = "A very basic flake";

  inputs = {
    haskellNix.url = "github:input-output-hk/haskell.nix";
    nixpkgs.follows = "haskellNix/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils, haskellNix }:
  flake-utils.lib.eachSystem [ "x86_64-linux" ] (system:
  let
    pname = "schseme";
    compiler-nix-name = "ghc8107";
    index-state = "2022-06-15T00:00:00Z";

    mkProject = self: self.haskell-nix.cabalProject' {
      inherit index-state compiler-nix-name;

      src = ./.;
      shell.tools = {
        cabal = { inherit index-state; };
        hlint = { inherit index-state; };
        haskell-language-server = { inherit index-state; };
      };

      shell.buildInputs = with self; [
        nixpkgs-fmt
      ];
    };

    overlays = [
      haskellNix.overlay
      (self: super: { ${pname} = mkProject self; })
    ];

    pkgs = import nixpkgs {
      inherit system overlays;
      inherit (haskellNix) config;
    };

    flake = pkgs.${pname}.flake {};
  in flake // rec {
    packages = {
      "${pname}" = flake.packages."${pname}:exe:${pname}";
      default = packages.${pname};
    };

    apps = {
      ${pname} = flake-utils.lib.mkApp {
        drv = packages.${pname};
        exePath = "/bin/${pname}";
      };
      default = apps.${pname};
    };

    checks = {
      inherit (packages) default;
      hlint = pkgs.runCommandLocal "hlint-check-${pname}" {
        inherit (flake.devShell) nativeBuildInputs;
      } ''
        hlint ${./app/Main.hs} | tee $out
      '';
    };
  });
}
