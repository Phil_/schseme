# Parsing

- different bases
    - octal
    - hexadecimal
- character literals
- floats
- other scheme [types](https://www.schemers.org/Documents/Standards/R5RS/HTML/r5rs-Z-H-9.html#%_sec_6.2.1)
- deep comparison equal?
- cond
- case
- string functions
